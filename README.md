**What is this script for?**  
This is the script that downloads Grafana monitoring dashboards from FASER experiment. It creates folder in the current directory and all the plots will be downloaded to this folder. Name of the folder uses start and end date in format of UNIX timestamp (number of seconds since 1970-01-01 00:00:00 UTC). It has to be used on lxplus otherwise it will not work because it uses Kerberos authentication (you need to have valid Kerberos ticket). 

**How to use it?**   
Usage: `./download-dashboard.sh <type of plots>  <start date> <end date>`.  
`<type of plots>` can be one of three options (case sensitivie): 
  - EHN1 - downloads plots from EHN1
  - TI12 - downloads plots from TI12
  - both - downloads plots both from EHN1 and TI12


Date is in format YYYY-MM-DD hh:mm:ss. Time is in UTC.    
Example: `./download-dashboard.sh both 2021-05-04 12:00:00 2021-05-06 12:00:00`.   
These informations are displayed in terminal if the script is called without parameters or any number of parameters not equal to four.   

In case you run the script with, let's say, TI12 option for a given time range, the script creates folder called `grafana_<UNIX_start_date>_<UNIX_end_date>` and inside it, there will be folder TI12 with TI12 plots. If you later decide to run the script for the same time range for EHN1, the EHN1 folder is created inside already existing grafana folder at the same level as the folder with TI12 data. If you want to run the script again for the same time range and folders TI12/EHN1 exist, it will ask you to remove or rename the folders. This is to avoid unnecessary waiting times in case you select the wrong option. If you have any comments or you wanted to add some additional plots, just write me an e-mail (ondrej.theiner@cern.ch), or you can also do modifications yourself.


    

