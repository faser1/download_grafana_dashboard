#!/bin/bash

print_help () {
  echo "The first argument specifies what should be downloaded:" 
  echo "  -for downloading plots from EHN1 use 'EHN1'"
  echo "  -for downloading plots from TI12, use 'TI12'"
  echo "  -for downloading plots both from TI12 and EHN1, use 'both'"
  echo "Usage: ${0} <EHN1/TI12/both> <start date> <end date>"
  echo "Date is in format YYYY-MM-DD hh:mm:ss. Time is in UTC."
  echo "Example: ${0} both 2021/05/04 12:00:00 2021/05/06 12:00:00"
  exit
}

if [[ $# -ne 5 ]] ; then
  print_help
fi

FROM=`date --date=${2}' '${3}' +0000' +%s`
TO=`date --date=${4}' '${5}' +0000' +%s`
DIR='./grafana_'${FROM}'_'${TO}

if [ ! -d "$DIR" ]; then
  mkdir $DIR
  cd $DIR
else
  cd $DIR
fi

download_TI12 () {
  D1='https://faser-grafana.web.cern.ch/render/d-solo/VT7LpZwGk/tracker-hv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=2&width=1000&height=500&tz=Europe%2FBerlin'
  D2='https://faser-grafana.web.cern.ch/render/d-solo/VT7LpZwGk/tracker-hv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D3='https://faser-grafana.web.cern.ch/render/d-solo/-_H_TWQGk/tracker-lv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D4='https://faser-grafana.web.cern.ch/render/d-solo/-_H_TWQGk/tracker-lv?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&var-layer=All&panelId=9&width=1000&height=500&tz=Europe%2FBerlin'
  D5='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FBerlin'
  D6='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D7='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D8='https://faser-grafana.web.cern.ch/render/d-solo/bjXVKWwGz/tracker-temperatures?orgId=1&var-station=All&var-layer=All&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FBerlin'
  D9='https://faser-grafana.web.cern.ch/render/d-solo/2s1I9swGk/trb-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&var-station=All&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D10='https://faser-grafana.web.cern.ch/render/d-solo/o6rynWwGk/scintillators-oracle?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FBerlin'
  D11='https://faser-grafana.web.cern.ch/render/d-solo/o6rynWwGk/scintillators-oracle?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FBerlin'
  D12='https://faser-grafana.web.cern.ch/render/d-solo/Mc0YFywMk/pdu-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FBerlin'
  D13='https://faser-grafana.web.cern.ch/render/d-solo/Mc0YFywMk/pdu-status?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=7&width=1000&height=500&tz=Europe%2FBerlin'
  D14='https://faser-grafana.web.cern.ch/render/d-solo/fmYHgs9Gz/rack-environment?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FBerlin'
  D15='https://faser-grafana.web.cern.ch/render/d-solo/fmYHgs9Gz/rack-environment?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FBerlin'

  N=15
  auth-get-sso-cookie -u $D1 -o cookie.txt
  echo "Downloading TI12 1/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D1 > hv_voltage.png
  echo "Downloading TI12 2/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D2 > hv_current.png
  echo "Downloading TI12 3/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D3 > lv_idd.png
  echo "Downloading TI12 4/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D4 > lv_icc.png
  echo "Downloading TI12 5/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D5 > temperatures-SCT_A.png
  echo "Downloading TI12 6/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D6 > temperatures_SCT_B.png
  echo "Downloading TI12 7/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D7 > temperatures_frame.png
  echo "Downloading TI12 8/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D8 > humidity.png
  echo "Downloading TI12 9/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D9 > temperatures_TRB.png
  echo "Downloading TI12 10/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D10 > scintillator_timing.png
  echo "Downloading TI12 11/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D11 > scintillators_veto.png
  echo "Downloading TI12 12/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D12 > pdu_current.png
  echo "Downloading TI12 13/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D13 > pdu_loads.png
  echo "Downloading TI12 14/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D14 > rack_temperatures.png
  echo "Downloading TI12 15/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D15 > trigger_rate.png

  rm cookie.txt
}

download_EHN1 () {
  D1='https://faser-grafana.web.cern.ch/render/d-solo/myldGWW7k/ehn1-tracker-hv-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FZurich'
  D2='https://faser-grafana.web.cern.ch/render/d-solo/pcakMZWnz/ehn1-tracker-lv-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FZurich'
  D3='https://faser-grafana.web.cern.ch/render/d-solo/pcakMZWnz/ehn1-tracker-lv-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=5&width=1000&height=500&tz=Europe%2FZurich'
  D4='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FZurich'
  D5='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=7&width=1000&height=500&tz=Europe%2FZurich'
  D6='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=4&width=1000&height=500&tz=Europe%2FZurich'
  D7='https://faser-grafana.web.cern.ch/render/d-solo/6wRJAnWnz/testbeam-tracker-temperatures?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FZurich'
  D8='https://faser-grafana.web.cern.ch/render/d-solo/fs_rWWWnz/testbeam-scintillators-mpod?orgId=1&from='${FROM}'000&to='${TO}'000&panelId=6&width=1000&height=500&tz=Europe%2FZurich'
  D9='https://faser-grafana.web.cern.ch/render/d-solo/r1RvM7W7k/testbeam-trigger?orgId=1&refresh=10s&from='${FROM}'000&to='${TO}'000&panelId=2&width=1000&height=500&tz=Europe%2FZurich'
  
  N=9
  auth-get-sso-cookie -u $D1 -o cookie.txt
  echo "Downloading EHN1 1/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D1 > hv_voltage.png
  echo "Downloading EHN1 2/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D2 > lv_digital.png
  echo "Downloading EHN1 3/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D3 > lv_analog.png
  echo "Downloading EHN1 4/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D4 > temperatures-NtcA.png
  echo "Downloading EHN1 5/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D5 > temperatures_NtcB.png
  echo "Downloading EHN1 6/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D6 > temperatures_frame.png
  echo "Downloading EHN1 7/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D7 > humidity.png
  echo "Downloading EHN1 8/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D8 > calorimeter.png
  echo "Downloading EHN1 9/$N"
  curl -L --cookie cookie.txt --cookie-jar cookie.txt $D9 > trigger_rate.png

  rm cookie.txt
}

if [[ ${1} == "TI12" ]]; then
  if [ ! -d "./TI12" ]; then
    mkdir ./TI12
    cd ./TI12
    download_TI12
    cd ../
  else
    echo "WARNING: Folder with name TI12 already exists! It means that you have probably"
    echo "already downloaded grafana dashbords from TI12 in given timerange."
    echo "If you want to continue, remove or rename folder $DIR/TI12."
  fi

elif [[ ${1} == "EHN1" ]]; then
  if [ ! -d "./EHN1" ]; then
    mkdir ./EHN1
    cd ./EHN1
    download_EHN1
    cd ../
  else
    echo "WARNING: Folder with name EHN1 already exists! It means that you have probably"
    echo "already downloaded grafana dashbords from EHN1 in given timerange."
    echo "If you want to continue, remove or rename folder $DIR/EHN1."
  fi

elif [[ ${1} == "both" ]]; then
  if [ ! -d "./TI12" ]; then
    mkdir ./TI12
    cd ./TI12
    download_TI12
    cd ../
  else
    echo "WARNING: Folder with name TI12 already exists! It means that you have probably"
    echo "already downloaded grafana dashbords from TI12 in given timerange."
    echo "If you want to continue, remove or rename folder $DIR/TI12."
  fi

  if [ ! -d "./EHN1" ]; then
    mkdir ./EHN1
    cd ./EHN1
    download_EHN1
    cd ../
  else
    echo "WARNING: Folder with name EHN1 already exists! It means that you have probably"
    echo "already downloaded grafana dashbords from EHN1 in given timerange."
    echo "If you want to continue, remove or rename folder $DIR/EHN1."
  fi

else
  echo "WARNING: Unknown option."
  print_help
fi

cd ../
echo 'Finished!' 
